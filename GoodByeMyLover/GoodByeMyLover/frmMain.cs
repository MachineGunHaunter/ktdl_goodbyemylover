﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GoodByeMyLover.dto;
using System.Collections;
using System.Text.RegularExpressions;

namespace GoodByeMyLover
{
    public partial class frmMain : Form
    {
        // 2 tap tin heart-c va hear-h se luu o 2 bien nay
        Dictionary<int, heart> DuLieuC = new Dictionary<int, heart>();
        Dictionary<int, heart> DuLieuH = new Dictionary<int, heart>();
        Dictionary<int, heart> DuLieuIntegration = new Dictionary<int, heart>();
        Dictionary<int, heart> DuLieuClean = new Dictionary<int, heart>();
        Dictionary<int, heart> DuLieuStandard = new Dictionary<int, heart>();

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            ThemDulieuVaoCombobox();

            //Load du lieu 2 tap tin
            LoadDuLieu("heart-c.arff", DuLieuC);
            LoadDuLieu("heart-h.arff", DuLieuH);
            LoadDuLieu("heart-integration-auto.arff", DuLieuIntegration);
        }

        #region Hau Lam Cau Truy van a
        void ThemDulieuVaoCombobox()
        {
            //Tim trong thu DuLieu lay ten 2 file heart-c va heart-h

            DirectoryInfo d = new DirectoryInfo(@"../../DuLieu");
            FileInfo[] Files = d.GetFiles("*.arff");
            string str = "";
            DataTable dtblDataSource = new DataTable();
            dtblDataSource.Columns.Add("DisplayMember");
            dtblDataSource.Columns.Add("ValueMember");

            foreach (FileInfo file in Files)
            {
                str = file.Name;
                Console.WriteLine(str);
                cbbChonTapTin.Items.Add(str);
                dtblDataSource.Rows.Add(str, str);
            }

            cbbChonTapTin.Items.Clear();
            cbbChonTapTin.DataSource = dtblDataSource;
            cbbChonTapTin.DisplayMember = "DisplayMember";
            cbbChonTapTin.ValueMember = "ValueMember";
        }

        private void btnTruyVan_Click(object sender, EventArgs e)
        {
            try
            {
                //Kiem tra nhap chi muc
                if (txtChiMuc.Text != "")
                {
                    int keyTim = Int16.Parse(txtChiMuc.Text);
                    //Kiem tra trong combobox la file .arff nao
                    if (cbbChonTapTin.Text == "heart-h.arff")
                    {
                        //Tim chi muc theo key
                        if (DuLieuH.ContainsKey(keyTim))
                        {
                            String KQ = DuLieuH[keyTim].Age + "," + DuLieuH[keyTim].Sex + "," + DuLieuH[keyTim].Cp + "," + DuLieuH[keyTim].Trestbps + "," +
                             DuLieuH[keyTim].Chol + "," + DuLieuH[keyTim].Fbs + "," + DuLieuH[keyTim].Restecg + "," + DuLieuH[keyTim].Thalach + "," + DuLieuH[keyTim].Exang
                              + "," + DuLieuH[keyTim].Oldpeak + "," + DuLieuH[keyTim].Slope + "," + DuLieuH[keyTim].Ca + "," + DuLieuH[keyTim].Thal + "," + DuLieuH[keyTim].Num;
                            txtKQTruyVan.Text = KQ;
                        }
                    }
                    else
                    {
                        if (DuLieuC.ContainsKey(keyTim))
                        {
                            String KQ = DuLieuC[keyTim].Age + "," + DuLieuC[keyTim].Sex + "," + DuLieuC[keyTim].Cp + "," + DuLieuC[keyTim].Trestbps + "," +
                            DuLieuC[keyTim].Chol + "," + DuLieuC[keyTim].Fbs + "," + DuLieuC[keyTim].Restecg + "," + DuLieuC[keyTim].Thalach + "," + DuLieuC[keyTim].Exang
                              + "," + DuLieuC[keyTim].Oldpeak + "," + DuLieuC[keyTim].Slope + "," + DuLieuC[keyTim].Ca + "," + DuLieuC[keyTim].Thal + "," + DuLieuC[keyTim].Num;
                            txtKQTruyVan.Text = KQ;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void LoadDuLieu(String TenTapTin, Dictionary<int, heart> DuLieu)
        {

            String DuongDanFileTruyVan = "../../DuLieu/" + TenTapTin;

            //Bien lines chua tat ca cac dong trong file .arff
            string[] lines = File.ReadAllLines(DuongDanFileTruyVan);


            //Bien kiem tra duyet den dong co chua @data chua
            bool KhuVucDuLieu = false;
            int KeyCuaDuLieu = 0;

            if (lines.Length > 0)
            {
                //Duyet theo tung dong
                foreach (string dong in lines)
                {
                    //Neu toi vung du lieu 
                    if (KhuVucDuLieu == true)
                    {
                        String DuLieuMoiDong = dong;
                        //Console.Out.WriteLine(Tuoi);

                        //Tao doi tuong heart va them vao bien DuLieuC hoac H kieu Dictionary<int,heart>
                        heart h = new heart();
                        int TungViTri = 0;

                        string[] words = DuLieuMoiDong.Split(',');
                        foreach (string word in words)
                        {
                            string TungTu = word;
                            if (word == "?")
                            {
                                TungTu = null;
                            }
                            switch (TungViTri)
                            {
                                case 0:
                                    if (TungTu == null)
                                    {
                                        h.Age = "?";
                                    }
                                    else
                                    {
                                        h.Age = TungTu;
                                    }
                                    break;
                                case 1:
                                    if (TungTu == null)
                                    {
                                        h.Sex = "?";
                                    }
                                    else
                                    {
                                        h.Sex = TungTu;
                                    }
                                    break;
                                case 2:
                                    if (TungTu == null)
                                    {
                                        h.Cp = "?";
                                    }
                                    else
                                    {
                                        h.Cp = TungTu;
                                    }
                                    break;
                                case 3:
                                    if (TungTu == null)
                                    {
                                        h.Trestbps = "?";
                                    }
                                    else
                                    {
                                        h.Trestbps = TungTu;
                                    }
                                    break;
                                case 4:
                                    if (TungTu == null)
                                    {
                                        h.Chol = "?";
                                    }
                                    else
                                    {
                                        h.Chol = TungTu;
                                    }
                                    break;
                                case 5:
                                    if (TungTu == null)
                                    {
                                        h.Fbs = "?";
                                    }
                                    else
                                    {
                                        h.Fbs = TungTu;
                                    }
                                    break;
                                case 6:
                                    if (TungTu == null)
                                    {
                                        h.Restecg = "?";
                                    }
                                    else
                                    {
                                        h.Restecg = TungTu;
                                    }
                                    break;
                                case 7:
                                    if (TungTu == null)
                                    {
                                        h.Thalach = "?";
                                    }
                                    else
                                    {
                                        h.Thalach = TungTu;
                                    }
                                    break;
                                case 8:
                                    if (TungTu == null)
                                    {
                                        h.Exang = "?";
                                    }
                                    else
                                    {
                                        h.Exang = TungTu;
                                    }
                                    break;
                                case 9:
                                    if (TungTu == null)
                                    {
                                        h.Oldpeak = "?";
                                    }
                                    else
                                    {
                                        h.Oldpeak = TungTu;
                                    }
                                    break;
                                case 10:
                                    if (TungTu == null)
                                    {
                                        h.Slope = "?";
                                    }
                                    else
                                    {
                                        h.Slope = TungTu;
                                    }
                                    break;
                                case 11:
                                    if (TungTu == null)
                                    {
                                        h.Ca = "?";
                                    }
                                    else
                                    {
                                        h.Ca = TungTu;
                                    }
                                    break;
                                case 12:
                                    if (TungTu == null)
                                    {
                                        h.Thal = "?";
                                    }
                                    else
                                    {
                                        h.Thal = TungTu;
                                    }
                                    break;
                                case 13:
                                    if (TungTu == null)
                                    {
                                        h.Num = "?";
                                    }
                                    else
                                    {
                                        h.Num = TungTu;
                                    }
                                    break;
                            }

                            TungViTri++;

                        }
                        DuLieu.Add(KeyCuaDuLieu, h);
                        KeyCuaDuLieu++;
                    }
                    if (dong.IndexOf("@data") >= 0)
                    {
                        KhuVucDuLieu = true;
                    }
                }
            }
            Console.WriteLine(KeyCuaDuLieu);
        }

        void TichHop()
        {
            //Xu ly file c
            String DuongDanFileTruyVan = "../../DuLieu/heart-c.arff";

            //Bien lines chua tat ca cac dong trong file .arff
            string[] lines = File.ReadAllLines(DuongDanFileTruyVan);

            ArrayList KhaiBao = new ArrayList();
            bool ToiVungDuLieu = false;
            bool ToiVungTieuDe = false;
            StreamWriter OurStream;
            OurStream = File.CreateText("../../DuLieu/heart-integration-auto.arff");

            if (lines.Length > 0)
            {
                //Duyet theo tung dong
                foreach (String dong in lines)
                {
                    if (ToiVungTieuDe == true && dong != "" && ToiVungDuLieu == false)
                    {
                        string MoiDong = dong;

                        var charsToRemove = new string[] { "'" };
                        foreach (var c in charsToRemove)
                        {
                            MoiDong = MoiDong.Replace(c, string.Empty);
                        }
                        OurStream.WriteLine(MoiDong);
                    }
                    if (ToiVungDuLieu == true && dong != "")
                    {
                        string MoiDong = dong;

                        OurStream.WriteLine(MoiDong);
                        ToiVungDuLieu = true;
                    }
                    if (dong.IndexOf("@data") >= 0)
                    {
                        ToiVungDuLieu = true;
                    }
                    if (dong.IndexOf("@relation") >= 0)
                    {
                        string MoiDong = dong;

                        OurStream.WriteLine(MoiDong);
                        ToiVungTieuDe = true;
                    }
                }

                //Xu ly file h

                String DuongDanFileTruyVanH = "../../DuLieu/heart-h.arff";

                //Bien lines chua tat ca cac dong trong file .arff
                string[] linesH = File.ReadAllLines(DuongDanFileTruyVanH);

                ArrayList KhaiBaoH = new ArrayList();
                bool ToiVungDuLieuH = false;

                if (linesH.Length > 0)
                {
                    //Duyet theo tung dong
                    foreach (string dong in linesH)
                    {
                        if (ToiVungDuLieuH == true && dong != "")
                        {
                            OurStream.WriteLine(dong);
                            //KhaiBao.Add(dong);
                        }
                        if (dong.IndexOf("@data") >= 0)
                        {
                            ToiVungDuLieuH = true;
                        }
                    }
                }
            }
            OurStream.Close();
            MessageBox.Show("Tích hợp file heart-integration-auto.arff thành công");
            Console.WriteLine("Created File!");

            //foreach (string i in KhaiBao)
            //{
            //    Console.WriteLine(i);
            //}
        }
        #endregion

        private void btnTichHop_Click(object sender, EventArgs e)
        {
            TichHop();
        }

        struct DuLieuThieu
        {
            public int dong;
            public String cot;
        }

        private void btnClean_Click(object sender, EventArgs e)
        {
            DuLieuClean = DuLieuIntegration;
            //Cot chol
            int TongGiaTriCotChol = 0;
            int TongCotChol = 0;
            int GiaTriTrungBinhChol = 0;

            //Cot trestbps
            int TongGiaTriCotTrestbps = 0;
            int TongCotTrestbps = 0;
            int GiaTriTrungBinhTrestbps = 0;

            //Cot slope
            slope s = new slope();
            var dSlope = new Dictionary<string, int>();

            //Cot thal
            thal t = new thal();
            var dThal = new Dictionary<string, int>();

            //Cot fps
            fps f = new fps();
            var dF = new Dictionary<string, int>();

            //cot restecg
            restecg res = new restecg();
            var dRes = new Dictionary<string, int>();

            //cot exang
            exang ex = new exang();
            var dEx = new Dictionary<string, int>();

            //Cot thalach
            float TongGiaTriCotThalach = 0;
            float TongCotThalach = 0;
            float GiaTriTrungBinhThalach = 0;

            //Cot ca
            float TongGiaTriCotCa = 0;
            float TongCotCa = 0;
            float GiaTriTrungBinhCa = 0;

            Dictionary<int, String> d = new Dictionary<int, String>();
            int Dem = 0;

            //List luu lai vi tri thieu
            List<DuLieuThieu> ListMiss = new List<DuLieuThieu>();

            for (int i = 0; i < DuLieuIntegration.Count; i++)
            {
                //Chol
                if (DuLieuIntegration[i].Chol == "?")
                {
                    DuLieuThieu DLT = new DuLieuThieu();
                    DLT.dong = i;
                    DLT.cot = "chol";
                    ListMiss.Add(DLT);
                }
                else
                {
                    TongGiaTriCotChol = TongGiaTriCotChol += int.Parse(DuLieuIntegration[i].Chol);
                    TongCotChol++;
                }
                //restecg
                if (DuLieuIntegration[i].Restecg == "?")
                {
                    DuLieuThieu DLT = new DuLieuThieu();
                    DLT.dong = i;
                    DLT.cot = "restecg";
                    ListMiss.Add(DLT);
                }
                else
                {
                    if (DuLieuIntegration[i].Slope == "left_vent_hyper")
                    {
                        res.Left_vent_hyper++;
                    }
                    else if (DuLieuIntegration[i].Slope == "normal")
                    {
                        res.Normal++;
                    }
                    else if (DuLieuIntegration[i].Slope == "st_t_wave_abnormality")
                    {
                        res.St_t_wave_abnormality++;
                    }
                }
                //exang
                if (DuLieuIntegration[i].Exang == "?")
                {
                    DuLieuThieu DLT = new DuLieuThieu();
                    DLT.dong = i;
                    DLT.cot = "exang";
                    ListMiss.Add(DLT);
                }
                else
                {
                    if (DuLieuIntegration[i].Exang == "no")
                    {
                        ex.No++;
                    }
                    else if (DuLieuIntegration[i].Exang == "yes")
                    {
                        ex.Yes++;
                    }
                }
                //thalach
                if (DuLieuIntegration[i].Thalach == "?")
                {
                    DuLieuThieu DLT = new DuLieuThieu();
                    DLT.dong = i;
                    DLT.cot = "thalach";
                    ListMiss.Add(DLT);
                }
                else
                {
                    TongGiaTriCotThalach = TongGiaTriCotThalach += int.Parse(DuLieuIntegration[i].Thalach);
                    TongCotThalach++;
                }
                //Ca
                if (DuLieuIntegration[i].Ca == "?")
                {
                    DuLieuThieu DLT = new DuLieuThieu();
                    DLT.dong = i;
                    DLT.cot = "ca";
                    ListMiss.Add(DLT);
                }
                else
                {
                    TongGiaTriCotCa = TongGiaTriCotCa += float.Parse(DuLieuIntegration[i].Ca);
                    TongCotCa++;
                }
                //trestbps
                if (DuLieuIntegration[i].Trestbps == "?")
                {
                    DuLieuThieu DLT = new DuLieuThieu();
                    DLT.dong = i;
                    DLT.cot = "trestbps";
                    ListMiss.Add(DLT);
                }
                else
                {
                    TongGiaTriCotTrestbps = TongGiaTriCotTrestbps += int.Parse(DuLieuIntegration[i].Trestbps);
                    TongCotTrestbps++;
                }
                //Slope
                if (DuLieuIntegration[i].Slope == "?")
                {
                    DuLieuThieu DLT = new DuLieuThieu();
                    DLT.dong = i;
                    DLT.cot = "slope";
                    ListMiss.Add(DLT);
                }
                else
                {
                    if (DuLieuIntegration[i].Slope == "up")
                    {
                        s.Up++;
                    }
                    else if (DuLieuIntegration[i].Slope == "flat")
                    {
                        s.Flash++;
                    }
                    else if (DuLieuIntegration[i].Slope == "down")
                    {
                        s.Down++;
                    }
                }

                //Thal
                if (DuLieuIntegration[i].Thal == "?")
                {
                    DuLieuThieu DLT = new DuLieuThieu();
                    DLT.dong = i;
                    DLT.cot = "thal";
                    ListMiss.Add(DLT);
                }
                else
                {
                    if (DuLieuIntegration[i].Thal == "fixed_defect")
                    {
                        t.Fixed_defect++;
                    }
                    else if (DuLieuIntegration[i].Thal == "normal")
                    {
                        t.Normal++;
                    }
                    else if (DuLieuIntegration[i].Thal == "reversable_defect")
                    {
                        t.Reversable_defect++;
                    }
                }
                //fbs
                if (DuLieuIntegration[i].Fbs == "?")
                {
                    DuLieuThieu DLT = new DuLieuThieu();
                    DLT.dong = i;
                    DLT.cot = "fbs";
                    ListMiss.Add(DLT);
                }
                else
                {
                    if (DuLieuIntegration[i].Fbs == "t")
                    {
                        f.T++;
                    }
                    else if (DuLieuIntegration[i].Fbs == "f")
                    {
                        f.F++;
                    }
                }
            }


            //Tim min in Slope
            dSlope.Add("up", s.Up);
            dSlope.Add("flat", s.Flash);
            dSlope.Add("down", s.Down);
            var keyR = dSlope.Aggregate((l, r) => l.Value < r.Value ? l : r).Key;
            Console.WriteLine("Gia tri slope min: " + keyR);

            //Tim min in Thal
            dThal.Add("fixed_defect", t.Fixed_defect);
            dThal.Add("normal", t.Normal);
            dThal.Add("reversable_defect", t.Reversable_defect);
            var keyT = dThal.Aggregate((l, r) => l.Value < r.Value ? l : r).Key;
            Console.WriteLine("Gia tri Thal min: " + keyT);

            //Tim min in fbs
            dF.Add("t", f.T);
            dF.Add("f", f.F);
            var keyF = dF.Aggregate((l, r) => l.Value < r.Value ? l : r).Key;
            Console.WriteLine("Gia tri Fbs min: " + keyF);

            //Tim min in restecg
            dRes.Add("left_vent_hyper", res.Left_vent_hyper);
            dRes.Add("normal", res.Normal);
            dRes.Add("st_t_wave_abnormality", res.St_t_wave_abnormality);
            var keyRestecg = dRes.Aggregate((l, r) => l.Value < r.Value ? l : r).Key;
            Console.WriteLine("Gia tri restecg min: " + keyRestecg);

            //Tim min in exang
            dEx.Add("yes", ex.Yes);
            dEx.Add("no", ex.No);
            var keyExang = dEx.Aggregate((l, r) => l.Value < r.Value ? l : r).Key;
            Console.WriteLine("Gia tri exang min: " + keyExang);

            //Xu ly thalach
            GiaTriTrungBinhThalach = TongGiaTriCotThalach / TongCotThalach;

            //Xu ly Chol
            GiaTriTrungBinhChol = TongGiaTriCotChol / TongCotChol;

            //Xy ly Ca
            GiaTriTrungBinhCa = TongGiaTriCotCa / TongCotCa;

            //Xu ly Trestbps
            GiaTriTrungBinhTrestbps = TongGiaTriCotTrestbps / TongCotTrestbps;

            for (int i = 0; i < ListMiss.Count; i++)
            {
                //Console.WriteLine("dong thieu: " + ListMiss[i].dong + " - cot thieu: " + ListMiss[i].cot);

                if (ListMiss[i].cot == "trestbps")
                {
                    DuLieuClean[ListMiss[i].dong].Trestbps = GiaTriTrungBinhTrestbps.ToString();
                }
                if (ListMiss[i].cot == "chol")
                {
                    DuLieuClean[ListMiss[i].dong].Chol = GiaTriTrungBinhChol.ToString();
                }
                if (ListMiss[i].cot == "fbs")
                {
                    DuLieuClean[ListMiss[i].dong].Fbs = keyF;
                }
                if (ListMiss[i].cot == "slope")
                {
                    DuLieuClean[ListMiss[i].dong].Slope = keyR;
                }
                if (ListMiss[i].cot == "ca")
                {
                    DuLieuClean[ListMiss[i].dong].Ca = Math.Round(GiaTriTrungBinhCa).ToString();
                }
                if (ListMiss[i].cot == "thal")
                {
                    DuLieuClean[ListMiss[i].dong].Thal = keyT;
                }
                if (ListMiss[i].cot == "restecg")
                {
                    DuLieuClean[ListMiss[i].dong].Restecg = keyRestecg;
                }
                if (ListMiss[i].cot == "exang")
                {
                    DuLieuClean[ListMiss[i].dong].Exang = keyExang;
                }
                if (ListMiss[i].cot == "thalach")
                {
                    DuLieuClean[ListMiss[i].dong].Thalach = GiaTriTrungBinhThalach.ToString();
                }
                Dem++;
            }

            Console.WriteLine("Tong gia tri thieu: " + Dem);

            Console.WriteLine("Gia tri trung binh chol " + GiaTriTrungBinhChol);
            Console.WriteLine("Gia tri trung binh Ca " + Math.Round(GiaTriTrungBinhCa));
            Console.WriteLine("Gia tri trung binh Trestbps: " + GiaTriTrungBinhTrestbps);

            StreamWriter OurStream;
            OurStream = File.CreateText("../../DuLieu/heart-cleaned-auto.arff");

            OurStream.WriteLine("@relation hungarian-14-heart-disease");
            OurStream.WriteLine("@attribute age numeric");
            OurStream.WriteLine("@attribute sex { female,male}");

            OurStream.WriteLine("@attribute cp { typ_angina,asympt,non_anginal,atyp_angina}");
            OurStream.WriteLine("@attribute trestbps numeric");

            OurStream.WriteLine("@attribute chol numeric");
            OurStream.WriteLine("@attribute fbs { t,f}");
            OurStream.WriteLine("@attribute restecg { left_vent_hyper,normal,st_t_wave_abnormality}");
            OurStream.WriteLine("@attribute thalach numeric");
            OurStream.WriteLine("@attribute exang { no,yes}");
            OurStream.WriteLine("@attribute oldpeak numeric");
            OurStream.WriteLine("@attribute slope { up,flat,down}");
            OurStream.WriteLine("@attribute ca numeric");
            OurStream.WriteLine("@attribute thal { fixed_defect,normal,reversable_defect}");
            OurStream.WriteLine("@attribute num {<50,>50_1,>50_2,>50_3,>50_4}");
            OurStream.WriteLine("@data");

            foreach (KeyValuePair<int, heart> kvp in DuLieuClean)
            {
                String a1 = kvp.Value.Age;
                String a2 = kvp.Value.Sex;
                String a3 = kvp.Value.Cp;
                String a4 = kvp.Value.Trestbps;
                String a5 = kvp.Value.Chol;
                String a6 = kvp.Value.Fbs;
                String a7 = kvp.Value.Restecg;
                String a8 = kvp.Value.Thalach;
                String a9 = kvp.Value.Exang;
                String a10 = kvp.Value.Oldpeak;
                String a11 = kvp.Value.Slope;
                String a12 = kvp.Value.Ca;
                String a13 = kvp.Value.Thal;
                String a14 = kvp.Value.Num;

                String ChuoiXuatRa = a1 + "," + a2 + "," + a3 + "," + a4 + "," + a5 + "," + a6 + "," + a7 + "," + a8 + "," + a9 + "," + a10 + "," + a11 + "," + a12 + "," + a13 + "," + a14;
                OurStream.WriteLine(ChuoiXuatRa);
            }


            OurStream.Close();
            Console.WriteLine("Created File Clean!");
            MessageBox.Show("Làm sạch dữ liệu heart-cleaned-auto.arff thành công!");
        }

        private void btnChuanHoa_Click(object sender, EventArgs e)
        {
            DuLieuStandard = DuLieuClean;

            double sumAge = 0.00, meanAge = 0.00;
            double bigSumAge = 0.00, stdDevAge;
            //
            double sumTrestbps = 0.00, meanTrestbps = 0.00;
            double bigSumTrestbps = 0.00, stdDevTrestbps;
            //
            double sumChol = 0.00, meanChol = 0.00;
            double bigSumChol = 0.00, stdDevChol;
            //
            double sumThalach = 0.00, meanThalach = 0.00;
            double bigSumThalach = 0.00, stdDevThalach;
            //
            double sumOldpeak = 0.00, meanOldpeak = 0.00;
            double bigSumOldpeak = 0.00, stdDevOldpeak;
            //
            double sumCa = 0.00, meanCa = 0.00;
            double bigSumCa = 0.00, stdDevCa;

            foreach (KeyValuePair<int, heart> kvp in DuLieuStandard)
            {
                sumAge += Double.Parse(kvp.Value.Age);
                sumTrestbps += Double.Parse(kvp.Value.Trestbps);
                sumChol += Double.Parse(kvp.Value.Chol);
                sumThalach += Double.Parse(kvp.Value.Thalach);
                sumOldpeak += Double.Parse(kvp.Value.Oldpeak);
                sumCa += Double.Parse(kvp.Value.Ca);
            }
            meanAge = sumAge / DuLieuStandard.Count;
            meanTrestbps = sumTrestbps / DuLieuStandard.Count;
            meanChol = sumChol / DuLieuStandard.Count;
            meanThalach = sumThalach / DuLieuStandard.Count;
            meanOldpeak = sumOldpeak / DuLieuStandard.Count;
            meanCa = sumCa / DuLieuStandard.Count;

            // Calculate the total for the standard deviation
            for (int i = 0; i < DuLieuStandard.Count; i++)
            {
                bigSumAge += Math.Pow(Double.Parse(DuLieuStandard[i].Age) - meanAge, 2);
                bigSumTrestbps += Math.Pow(Double.Parse(DuLieuStandard[i].Trestbps) - meanTrestbps, 2);
                bigSumChol += Math.Pow(Double.Parse(DuLieuStandard[i].Chol) - meanChol, 2);
                bigSumThalach += Math.Pow(Double.Parse(DuLieuStandard[i].Thalach) - meanThalach, 2);
                bigSumOldpeak += Math.Pow(Double.Parse(DuLieuStandard[i].Oldpeak) - meanOldpeak, 2);
                bigSumCa += Math.Pow(Double.Parse(DuLieuStandard[i].Ca) - meanCa, 2);
            }
            // Now we can calculate the standard deviation
            stdDevAge = Math.Sqrt(bigSumAge / (DuLieuStandard.Count - 1));
            stdDevTrestbps = Math.Sqrt(bigSumTrestbps / (DuLieuStandard.Count - 1));
            stdDevChol = Math.Sqrt(bigSumChol / (DuLieuStandard.Count - 1));
            stdDevThalach = Math.Sqrt(bigSumThalach / (DuLieuStandard.Count - 1));
            stdDevOldpeak = Math.Sqrt(bigSumOldpeak / (DuLieuStandard.Count - 1));
            stdDevCa = Math.Sqrt(bigSumCa / (DuLieuStandard.Count - 1));

            foreach (KeyValuePair<int, heart> kvp in DuLieuStandard)
            {
                double kqAge = (Double.Parse(kvp.Value.Age) - meanAge) / stdDevAge;
                DuLieuStandard[kvp.Key].Age = kqAge.ToString();

                double kqTrestbps = (Double.Parse(kvp.Value.Trestbps) - meanTrestbps) / stdDevTrestbps;
                DuLieuStandard[kvp.Key].Trestbps = kqTrestbps.ToString();

                double kqChol = (Double.Parse(kvp.Value.Chol) - meanChol) / stdDevChol;
                DuLieuStandard[kvp.Key].Chol = kqChol.ToString();

                double kqThalach = (Double.Parse(kvp.Value.Thalach) - meanThalach) / stdDevThalach;
                DuLieuStandard[kvp.Key].Thalach = kqThalach.ToString();

                double kqOldpeak = (Double.Parse(kvp.Value.Oldpeak) - meanOldpeak) / stdDevOldpeak;
                DuLieuStandard[kvp.Key].Oldpeak = kqOldpeak.ToString();

                double kqCa = (Double.Parse(kvp.Value.Ca) - meanCa) / stdDevCa;
                DuLieuStandard[kvp.Key].Ca = kqCa.ToString();
            }

            //tao file normal auto
            StreamWriter OurStream;
            OurStream = File.CreateText("../../DuLieu/heart-normal-auto.arff");

            OurStream.WriteLine("@relation hungarian-14-heart-disease");
            OurStream.WriteLine("@attribute age numeric");
            OurStream.WriteLine("@attribute sex { female,male}");

            OurStream.WriteLine("@attribute cp { typ_angina,asympt,non_anginal,atyp_angina}");
            OurStream.WriteLine("@attribute trestbps numeric");

            OurStream.WriteLine("@attribute chol numeric");
            OurStream.WriteLine("@attribute fbs { t,f}");
            OurStream.WriteLine("@attribute restecg { left_vent_hyper,normal,st_t_wave_abnormality}");
            OurStream.WriteLine("@attribute thalach numeric");
            OurStream.WriteLine("@attribute exang { no,yes}");
            OurStream.WriteLine("@attribute oldpeak numeric");
            OurStream.WriteLine("@attribute slope { up,flat,down}");
            OurStream.WriteLine("@attribute ca numeric");
            OurStream.WriteLine("@attribute thal { fixed_defect,normal,reversable_defect}");
            OurStream.WriteLine("@attribute num {<50,>50_1,>50_2,>50_3,>50_4}");
            OurStream.WriteLine("@data");

            foreach (KeyValuePair<int, heart> kvp in DuLieuStandard)
            {
                String a1 = kvp.Value.Age;
                String a2 = kvp.Value.Sex;
                String a3 = kvp.Value.Cp;
                String a4 = kvp.Value.Trestbps;
                String a5 = kvp.Value.Chol;
                String a6 = kvp.Value.Fbs;
                String a7 = kvp.Value.Restecg;
                String a8 = kvp.Value.Thalach;
                String a9 = kvp.Value.Exang;
                String a10 = kvp.Value.Oldpeak;
                String a11 = kvp.Value.Slope;
                String a12 = kvp.Value.Ca;
                String a13 = kvp.Value.Thal;
                String a14 = kvp.Value.Num;

                String ChuoiXuatRa = a1 + "," + a2 + "," + a3 + "," + a4 + "," + a5 + "," + a6 + "," + a7 + "," + a8 + "," + a9 + "," + a10 + "," + a11 + "," + a12 + "," + a13 + "," + a14;
                OurStream.WriteLine(ChuoiXuatRa);
            }

            OurStream.Close();
            Console.WriteLine("Created File Standard!");
            MessageBox.Show("Chuẩn hóa heart-normal-auto.arff thành công!");
        }

        private void btnKhongThayThe_Click(object sender, EventArgs e)
        {
            StreamWriter OurStream;
            OurStream = File.CreateText("../../DuLieu/heart-srswr.arff");

            OurStream.WriteLine("@relation hungarian-14-heart-disease");
            OurStream.WriteLine("@attribute age numeric");
            OurStream.WriteLine("@attribute sex { female,male}");

            OurStream.WriteLine("@attribute cp { typ_angina,asympt,non_anginal,atyp_angina}");
            OurStream.WriteLine("@attribute trestbps numeric");

            OurStream.WriteLine("@attribute chol numeric");
            OurStream.WriteLine("@attribute fbs { t,f}");
            OurStream.WriteLine("@attribute restecg { left_vent_hyper,normal,st_t_wave_abnormality}");
            OurStream.WriteLine("@attribute thalach numeric");
            OurStream.WriteLine("@attribute exang { no,yes}");
            OurStream.WriteLine("@attribute oldpeak numeric");
            OurStream.WriteLine("@attribute slope { up,flat,down}");
            OurStream.WriteLine("@attribute ca numeric");
            OurStream.WriteLine("@attribute thal { fixed_defect,normal,reversable_defect}");
            OurStream.WriteLine("@attribute num {<50,>50_1,>50_2,>50_3,>50_4}");
            OurStream.WriteLine("@data");

            int NuaDuLieu = DuLieuStandard.Count / 2;
            bool ktTongSoLuongMauChan = true;
            if (DuLieuStandard.Count % 2 == 1)
            {
                ktTongSoLuongMauChan = false;
            }

            for (int i = 0; i < NuaDuLieu; i++)
            {
                String a1 = DuLieuStandard[i].Age;
                String a2 = DuLieuStandard[i].Sex;
                String a3 = DuLieuStandard[i].Cp;
                String a4 = DuLieuStandard[i].Trestbps;
                String a5 = DuLieuStandard[i].Chol;
                String a6 = DuLieuStandard[i].Fbs;
                String a7 = DuLieuStandard[i].Restecg;
                String a8 = DuLieuStandard[i].Thalach;
                String a9 = DuLieuStandard[i].Exang;
                String a10 = DuLieuStandard[i].Oldpeak;
                String a11 = DuLieuStandard[i].Slope;
                String a12 = DuLieuStandard[i].Ca;
                String a13 = DuLieuStandard[i].Thal;
                String a14 = DuLieuStandard[i].Num;

                String ChuoiXuatRa = a1 + "," + a2 + "," + a3 + "," + a4 + "," + a5 + "," + a6 + "," + a7 + "," + a8 + "," + a9 + "," + a10 + "," + a11 + "," + a12 + "," + a13 + "," + a14;
                OurStream.WriteLine(ChuoiXuatRa);
            }

            if (ktTongSoLuongMauChan == true)
            {
                for (int i = 0; i < NuaDuLieu; i++)
                {
                    String a1 = DuLieuStandard[i].Age;
                    String a2 = DuLieuStandard[i].Sex;
                    String a3 = DuLieuStandard[i].Cp;
                    String a4 = DuLieuStandard[i].Trestbps;
                    String a5 = DuLieuStandard[i].Chol;
                    String a6 = DuLieuStandard[i].Fbs;
                    String a7 = DuLieuStandard[i].Restecg;
                    String a8 = DuLieuStandard[i].Thalach;
                    String a9 = DuLieuStandard[i].Exang;
                    String a10 = DuLieuStandard[i].Oldpeak;
                    String a11 = DuLieuStandard[i].Slope;
                    String a12 = DuLieuStandard[i].Ca;
                    String a13 = DuLieuStandard[i].Thal;
                    String a14 = DuLieuStandard[i].Num;

                    String ChuoiXuatRa = a1 + "," + a2 + "," + a3 + "," + a4 + "," + a5 + "," + a6 + "," + a7 + "," + a8 + "," + a9 + "," + a10 + "," + a11 + "," + a12 + "," + a13 + "," + a14;
                    OurStream.WriteLine(ChuoiXuatRa);
                }
            }
            else
            {
                for (int i = 0; i <= NuaDuLieu; i++)
                {
                    String a1 = DuLieuStandard[i].Age;
                    String a2 = DuLieuStandard[i].Sex;
                    String a3 = DuLieuStandard[i].Cp;
                    String a4 = DuLieuStandard[i].Trestbps;
                    String a5 = DuLieuStandard[i].Chol;
                    String a6 = DuLieuStandard[i].Fbs;
                    String a7 = DuLieuStandard[i].Restecg;
                    String a8 = DuLieuStandard[i].Thalach;
                    String a9 = DuLieuStandard[i].Exang;
                    String a10 = DuLieuStandard[i].Oldpeak;
                    String a11 = DuLieuStandard[i].Slope;
                    String a12 = DuLieuStandard[i].Ca;
                    String a13 = DuLieuStandard[i].Thal;
                    String a14 = DuLieuStandard[i].Num;

                    String ChuoiXuatRa = a1 + "," + a2 + "," + a3 + "," + a4 + "," + a5 + "," + a6 + "," + a7 + "," + a8 + "," + a9 + "," + a10 + "," + a11 + "," + a12 + "," + a13 + "," + a14;
                    OurStream.WriteLine(ChuoiXuatRa);
                }
            }
            OurStream.Close();
            Console.WriteLine("Created File heart-srsowr.arf!");
            MessageBox.Show("Lấy mẫu tự đồng thành file heart-srsowr.arf");
        }

        private void btnThayThe_Click(object sender, EventArgs e)
        {
            StreamWriter OurStream;
            OurStream = File.CreateText("../../DuLieu/heart-srsowr.arff");

            OurStream.WriteLine("@relation hungarian-14-heart-disease");
            OurStream.WriteLine("@attribute age numeric");
            OurStream.WriteLine("@attribute sex { female,male}");

            OurStream.WriteLine("@attribute cp { typ_angina,asympt,non_anginal,atyp_angina}");
            OurStream.WriteLine("@attribute trestbps numeric");

            OurStream.WriteLine("@attribute chol numeric");
            OurStream.WriteLine("@attribute fbs { t,f}");
            OurStream.WriteLine("@attribute restecg { left_vent_hyper,normal,st_t_wave_abnormality}");
            OurStream.WriteLine("@attribute thalach numeric");
            OurStream.WriteLine("@attribute exang { no,yes}");
            OurStream.WriteLine("@attribute oldpeak numeric");
            OurStream.WriteLine("@attribute slope { up,flat,down}");
            OurStream.WriteLine("@attribute ca numeric");
            OurStream.WriteLine("@attribute thal { fixed_defect,normal,reversable_defect}");
            OurStream.WriteLine("@attribute num {<50,>50_1,>50_2,>50_3,>50_4}");
            OurStream.WriteLine("@data");

            int NuaDuLieu = DuLieuStandard.Count / 2;
            bool ktTongSoLuongMauChan = true;
            if (DuLieuStandard.Count % 2 == 1)
            {
                ktTongSoLuongMauChan = false;
            }

            for (int i = 0; i < NuaDuLieu; i++)
            {
                String a1 = DuLieuStandard[i].Age;
                String a2 = DuLieuStandard[i].Sex;
                String a3 = DuLieuStandard[i].Cp;
                String a4 = DuLieuStandard[i].Trestbps;
                String a5 = DuLieuStandard[i].Chol;
                String a6 = DuLieuStandard[i].Fbs;
                String a7 = DuLieuStandard[i].Restecg;
                String a8 = DuLieuStandard[i].Thalach;
                String a9 = DuLieuStandard[i].Exang;
                String a10 = DuLieuStandard[i].Oldpeak;
                String a11 = DuLieuStandard[i].Slope;
                String a12 = DuLieuStandard[i].Ca;
                String a13 = DuLieuStandard[i].Thal;
                String a14 = DuLieuStandard[i].Num;

                String ChuoiXuatRa = a1 + "," + a2 + "," + a3 + "," + a4 + "," + a5 + "," + a6 + "," + a7 + "," + a8 + "," + a9 + "," + a10 + "," + a11 + "," + a12 + "," + a13 + "," + a14;
                OurStream.WriteLine(ChuoiXuatRa);
            }

            if (ktTongSoLuongMauChan == true)
            {
                for (int i = 0; i < NuaDuLieu; i++)
                {
                    String a1 = DuLieuStandard[i].Age;
                    String a2 = DuLieuStandard[i].Sex;
                    String a3 = DuLieuStandard[i].Cp;
                    String a4 = DuLieuStandard[i].Trestbps;
                    String a5 = DuLieuStandard[i].Chol;
                    String a6 = DuLieuStandard[i].Fbs;
                    if (a6 == "t")
                    {
                        a6 = "f";
                    }
                    else if (a6 == "f")
                    {
                        a6 = "t";
                    }
                    String a7 = DuLieuStandard[i].Restecg;
                    String a8 = DuLieuStandard[i].Thalach;
                    String a9 = DuLieuStandard[i].Exang;
                    String a10 = DuLieuStandard[i].Oldpeak;
                    String a11 = DuLieuStandard[i].Slope;
                    String a12 = DuLieuStandard[i].Ca;
                    String a13 = DuLieuStandard[i].Thal;
                    String a14 = DuLieuStandard[i].Num;

                    String ChuoiXuatRa = a1 + "," + a2 + "," + a3 + "," + a4 + "," + a5 + "," + a6 + "," + a7 + "," + a8 + "," + a9 + "," + a10 + "," + a11 + "," + a12 + "," + a13 + "," + a14;
                    OurStream.WriteLine(ChuoiXuatRa);
                }
            }
            else
            {
                for (int i = 0; i <= NuaDuLieu; i++)
                {
                    String a1 = DuLieuStandard[i].Age;
                    String a2 = DuLieuStandard[i].Sex;
                    String a3 = DuLieuStandard[i].Cp;
                    String a4 = DuLieuStandard[i].Trestbps;
                    String a5 = DuLieuStandard[i].Chol;
                    String a6 = DuLieuStandard[i].Fbs;
                    if (a6 == "t")
                    {
                        a6 = "f";
                    }
                    else if (a6 == "f")
                    {
                        a6 = "t";
                    }
                    String a7 = DuLieuStandard[i].Restecg;
                    String a8 = DuLieuStandard[i].Thalach;
                    String a9 = DuLieuStandard[i].Exang;
                    String a10 = DuLieuStandard[i].Oldpeak;
                    String a11 = DuLieuStandard[i].Slope;
                    String a12 = DuLieuStandard[i].Ca;
                    String a13 = DuLieuStandard[i].Thal;
                    String a14 = DuLieuStandard[i].Num;

                    String ChuoiXuatRa = a1 + "," + a2 + "," + a3 + "," + a4 + "," + a5 + "," + a6 + "," + a7 + "," + a8 + "," + a9 + "," + a10 + "," + a11 + "," + a12 + "," + a13 + "," + a14;
                    OurStream.WriteLine(ChuoiXuatRa);
                }
            }
            OurStream.Close();
            Console.WriteLine("Created File Standard!");
            MessageBox.Show("Lấy mẫu tự động heart-srswr.arff");
        }
    }
}
