﻿namespace GoodByeMyLover
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnTruyVan = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtKQTruyVan = new System.Windows.Forms.TextBox();
            this.txtChiMuc = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbbChonTapTin = new System.Windows.Forms.ComboBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.btnTichHop = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnClean = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.btnChuanHoa = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnKhongThayThe = new System.Windows.Forms.Button();
            this.btnThayThe = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(734, 411);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnTruyVan);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.txtKQTruyVan);
            this.tabPage1.Controls.Add(this.txtChiMuc);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.cbbChonTapTin);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(726, 385);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Đọc tập tin";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnTruyVan
            // 
            this.btnTruyVan.Location = new System.Drawing.Point(323, 42);
            this.btnTruyVan.Name = "btnTruyVan";
            this.btnTruyVan.Size = new System.Drawing.Size(75, 23);
            this.btnTruyVan.TabIndex = 6;
            this.btnTruyVan.Text = "Truy vấn";
            this.btnTruyVan.UseVisualStyleBackColor = true;
            this.btnTruyVan.Click += new System.EventHandler(this.btnTruyVan_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Thông tin dòng dữ liệu";
            // 
            // txtKQTruyVan
            // 
            this.txtKQTruyVan.Location = new System.Drawing.Point(153, 100);
            this.txtKQTruyVan.Name = "txtKQTruyVan";
            this.txtKQTruyVan.Size = new System.Drawing.Size(505, 20);
            this.txtKQTruyVan.TabIndex = 4;
            // 
            // txtChiMuc
            // 
            this.txtChiMuc.Location = new System.Drawing.Point(153, 44);
            this.txtChiMuc.Name = "txtChiMuc";
            this.txtChiMuc.Size = new System.Drawing.Size(121, 20);
            this.txtChiMuc.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(139, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Chọn chỉ mục (bắt đầu từ 0)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Chọn tập tin:";
            // 
            // cbbChonTapTin
            // 
            this.cbbChonTapTin.FormattingEnabled = true;
            this.cbbChonTapTin.Location = new System.Drawing.Point(153, 6);
            this.cbbChonTapTin.Name = "cbbChonTapTin";
            this.cbbChonTapTin.Size = new System.Drawing.Size(121, 21);
            this.cbbChonTapTin.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.btnTichHop);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(726, 385);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Tính hợp tự động";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(283, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Tích hợp tự động hai tập dữ liệu heart-h.arff và heart-c.arff";
            // 
            // btnTichHop
            // 
            this.btnTichHop.Location = new System.Drawing.Point(297, 13);
            this.btnTichHop.Name = "btnTichHop";
            this.btnTichHop.Size = new System.Drawing.Size(75, 23);
            this.btnTichHop.TabIndex = 0;
            this.btnTichHop.Text = "Tích Hợp";
            this.btnTichHop.UseVisualStyleBackColor = true;
            this.btnTichHop.Click += new System.EventHandler(this.btnTichHop_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnClean);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(726, 385);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Làm sạch tự động";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnClean
            // 
            this.btnClean.Location = new System.Drawing.Point(8, 35);
            this.btnClean.Name = "btnClean";
            this.btnClean.Size = new System.Drawing.Size(75, 23);
            this.btnClean.TabIndex = 1;
            this.btnClean.Text = "Làm Sạch";
            this.btnClean.UseVisualStyleBackColor = true;
            this.btnClean.Click += new System.EventHandler(this.btnClean_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(328, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(141, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "heart-cleaned-auto.arff";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(314, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Làm sạch tự động tập dữ liệu heart-integration-auto.arff thành:";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.btnChuanHoa);
            this.tabPage4.Controls.Add(this.label7);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(726, 385);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Chuẩn hóa tự động";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // btnChuanHoa
            // 
            this.btnChuanHoa.Location = new System.Drawing.Point(11, 26);
            this.btnChuanHoa.Name = "btnChuanHoa";
            this.btnChuanHoa.Size = new System.Drawing.Size(75, 23);
            this.btnChuanHoa.TabIndex = 1;
            this.btnChuanHoa.Text = "Chuẩn Hóa";
            this.btnChuanHoa.UseVisualStyleBackColor = true;
            this.btnChuanHoa.Click += new System.EventHandler(this.btnChuanHoa_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 10);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(261, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Chuẩn hóa tự động tập dữ liệu heart-cleaned-auto.arff";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.btnThayThe);
            this.tabPage5.Controls.Add(this.btnKhongThayThe);
            this.tabPage5.Controls.Add(this.label9);
            this.tabPage5.Controls.Add(this.label8);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(726, 385);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Lấy mẫu tự động";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 13);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(225, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Simple Random Sample Without Replacement";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 55);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(210, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Simple Random Sample With Replacement";
            // 
            // btnKhongThayThe
            // 
            this.btnKhongThayThe.Location = new System.Drawing.Point(11, 29);
            this.btnKhongThayThe.Name = "btnKhongThayThe";
            this.btnKhongThayThe.Size = new System.Drawing.Size(75, 23);
            this.btnKhongThayThe.TabIndex = 1;
            this.btnKhongThayThe.Text = "srswr";
            this.btnKhongThayThe.UseVisualStyleBackColor = true;
            this.btnKhongThayThe.Click += new System.EventHandler(this.btnKhongThayThe_Click);
            // 
            // btnThayThe
            // 
            this.btnThayThe.Location = new System.Drawing.Point(11, 71);
            this.btnThayThe.Name = "btnThayThe";
            this.btnThayThe.Size = new System.Drawing.Size(75, 23);
            this.btnThayThe.TabIndex = 1;
            this.btnThayThe.Text = "srsowr";
            this.btnThayThe.UseVisualStyleBackColor = true;
            this.btnThayThe.Click += new System.EventHandler(this.btnThayThe_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 411);
            this.Controls.Add(this.tabControl1);
            this.Name = "frmMain";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " Preprocessing";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtKQTruyVan;
        private System.Windows.Forms.TextBox txtChiMuc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbbChonTapTin;
        private System.Windows.Forms.Button btnTruyVan;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnTichHop;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnClean;
        private System.Windows.Forms.Button btnChuanHoa;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnThayThe;
        private System.Windows.Forms.Button btnKhongThayThe;
    }
}

