﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodByeMyLover.dto
{
    public class slope
    {
        private int _up;
        private int _flash;
        private int _down;

        public slope()
        {
            this._up = 0;
            this._flash = 0;
            this._down = 0;
        }

        public int Up
        {
            get
            {
                return _up;
            }

            set
            {
                _up = value;
            }
        }

        public int Flash
        {
            get
            {
                return _flash;
            }

            set
            {
                _flash = value;
            }
        }

        public int Down
        {
            get
            {
                return _down;
            }

            set
            {
                _down = value;
            }
        }
    }
}
