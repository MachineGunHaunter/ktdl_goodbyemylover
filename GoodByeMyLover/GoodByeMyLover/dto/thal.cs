﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodByeMyLover.dto
{
    public class thal
    {
        private int fixed_defect;
        private int normal;
        private int reversable_defect;

        public thal()
        {
            this.Fixed_defect = 0;
            this.Normal = 0;
            this.Reversable_defect = 0;
        }

        public int Fixed_defect
        {
            get
            {
                return fixed_defect;
            }

            set
            {
                fixed_defect = value;
            }
        }

        public int Normal
        {
            get
            {
                return normal;
            }

            set
            {
                normal = value;
            }
        }

        public int Reversable_defect
        {
            get
            {
                return reversable_defect;
            }

            set
            {
                reversable_defect = value;
            }
        }
    }
}
