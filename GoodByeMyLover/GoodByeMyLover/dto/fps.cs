﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodByeMyLover.dto
{
    public class fps
    {
        private int t;
        private int f;

        public int T
        {
            get
            {
                return t;
            }

            set
            {
                t = value;
            }
        }

        public int F
        {
            get
            {
                return f;
            }

            set
            {
                f = value;
            }
        }

        public fps()
        {
            this.T = 0;
            this.F = 0;
        }
    }
}
