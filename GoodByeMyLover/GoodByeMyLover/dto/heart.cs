﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoodByeMyLover.dto
{
    class heart
    {
        private String age;
        private String sex;
        private String cp;
        private String trestbps;
        private String chol;
        private String fbs;
        private String restecg;
        private String thalach;
        private String exang;
        private String oldpeak;
        private String slope;
        private String ca;
        private String thal;
        private String num;

        public String Age
        {
            get
            {
                return age;
            }

            set
            {
                age = value;
            }
        }

        public string Sex
        {
            get
            {
                return sex;
            }

            set
            {
                sex = value;
            }
        }

        public string Cp
        {
            get
            {
                return cp;
            }

            set
            {
                cp = value;
            }
        }

        public String Trestbps
        {
            get
            {
                return trestbps;
            }

            set
            {
                trestbps = value;
            }
        }

        public String Chol
        {
            get
            {
                return chol;
            }

            set
            {
                chol = value;
            }
        }

        public string Fbs
        {
            get
            {
                return fbs;
            }

            set
            {
                fbs = value;
            }
        }

        public string Restecg
        {
            get
            {
                return restecg;
            }

            set
            {
                restecg = value;
            }
        }

        public String Thalach
        {
            get
            {
                return thalach;
            }

            set
            {
                thalach = value;
            }
        }

        public string Exang
        {
            get
            {
                return exang;
            }

            set
            {
                exang = value;
            }
        }

        public String Oldpeak
        {
            get
            {
                return oldpeak;
            }

            set
            {
                oldpeak = value;
            }
        }

        public string Slope
        {
            get
            {
                return slope;
            }

            set
            {
                slope = value;
            }
        }

        public String Ca
        {
            get
            {
                return ca;
            }

            set
            {
                ca = value;
            }
        }

        public string Thal
        {
            get
            {
                return thal;
            }

            set
            {
                thal = value;
            }
        }

        public string Num
        {
            get
            {
                return num;
            }

            set
            {
                num = value;
            }
        }
    }
}
